""" Module to unit test the generate_feature_class function.

Validates:
    - The no fit function / no transform function edge cases
    - Fit / transform functions with > 2 arguments have the expected number of arguments
    - That fit / transform functions with < 2 arguments are indeed raised
    - That transform function does enable changing the Features Class internal attributes
    - That the output of the generate_feature_class function is indeed a FeatureTemplate child
"""
from typing import Any, Callable

import pandas as pd

from src.features.features import (
    ArgumentError, FeatureTemplate, generate_features_class
)


def assert_is_feature_template(TestClass: Any) -> None:
    """ Assert generate_features_class returned a Feature Template child class. This ensures it has
    a fit, transform, and fit_transform method.

    :param TestClass: class which should be a child of FeatureTemplate

    :raises TypeError: if the passed TestClass is not a child of the FeatureTemplate category
    """
    if not issubclass(TestClass, FeatureTemplate):
        raise TypeError(
            "The generate_features_class class factory should have returned a FeatureTemplate child"
            " class"
        )


def test_no_fit_no_transform(simple_fit_function: Callable, simple_transform_function: Callable):
    """ Test that the generate_features_class yield a FeatureTemplate object and handles the no
    fit, no transform edge cases.

    :param simple_fit_function: pytest fixture (see conftest)
    :param simple_transform_function: pytest fixture (see conftest)

    :raises TypeError: if returned class is not a FeatureTemplate child
    """
    for feature_class_args in [
        {},
        {'fit_function': simple_fit_function},
        {'transform_function': simple_transform_function},
        {'fit_function': simple_fit_function, 'transform_function': simple_transform_function},
    ]:
        FeatureClass = generate_features_class(
            **feature_class_args
        )
        assert_is_feature_template(FeatureClass)


def test_fit_and_transform(
    simple_fit_function: Callable,
    simple_transform_function: Callable,
    test_raw_data: pd.DataFrame,
    complex_fit_function: Callable,
    complex_transfom_function: Callable
):
    """ Test the generated Feature Classes fit and transform functions

    :param simple_fit_function: pytest fixture (see conftest)
    :param simple_transform_function: pytest fixture (see conftest)
    :param test_raw_data: pytest ficture (see conftest)
    :param complex_fit_function: pytest fixture (see conftest)
    :param complex_transfom_function: pytest fixture (see conftest)
    """
    SimpleFeatureClass = generate_features_class(
        fit_function=simple_fit_function,
        transform_function=simple_transform_function,
    )
    simple_feature_object = SimpleFeatureClass()
    simple_feature_object.fit(raw_data=test_raw_data)
    simple_feature_object.transform(raw_data=test_raw_data)

    trainable_args = ['vectorizer', 'mean_value', 'std_value']
    ComplexFeatureClass = generate_features_class(
        fit_function=complex_fit_function,
        transform_function=complex_transfom_function,
        trainable_args=trainable_args,
    )
    complex_feature_object = ComplexFeatureClass()
    complex_feature_object.fit(
        raw_data=test_raw_data,
        max_ngram_len=2,
    )
    for trainable_arg in trainable_args:
        if getattr(complex_feature_object, trainable_arg, None) is None:
            raise ValueError(
                f'After fit trainable argument {trainable_arg} value is None whilst a'
                ' non missing value would have been expected'
            )
    complex_feature_object.transform(
        raw_data=test_raw_data,
        value_threshold=0,
    )


def test_error_incorrect_args(
    incorrect_fit: Callable,
    simple_fit_function: Callable,
    simple_transform_function: Callable,
    incorrect_transform: Callable,
):
    """ Test that incorrect arguments are properly handled

    :param simple_fit_function: pytest fixture (see conftest)
    :param simple_transform_function: pytest fixture (see conftest)
    :param incorrect_fit: pytest ficture (see conftest)
    :param incorrect_transform: pytest fixture (see conftest)
    """
    for fit_function, transform_function in (
        (incorrect_fit, simple_transform_function),
        (simple_fit_function, incorrect_transform),
    ):
        try:
            generate_features_class(
                fit_function=fit_function,
                transform_function=transform_function,
            )
        except ArgumentError:
            pass
        else:
            raise ValueError(
                'A method with an incorrect number of argument was passed yet no ArgumentError'
                ' raised'
            )
    for fit_function, transform_function in (
        (simple_fit_function, 'uncallable'),
        ('uncallable', simple_transform_function),
    ):
        try:
            generate_features_class(
                fit_function=fit_function,
                transform_function=transform_function,
            )
        except TypeError:
            pass
        else:
            raise ValueError(
                'A non callable function was passed yet no TypeError was raised'
            )
