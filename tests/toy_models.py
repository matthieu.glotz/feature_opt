""" Toy model definition for package testing"""
import logging
import random as python_random

import keras
import numpy as np
import pandas as pd
import tensorflow as tf
from keras.wrappers.scikit_learn import KerasClassifier
from tensorflow.keras import layers
from tensorflow.keras.utils import to_categorical

from src.features.features import ArrayDataFrameMatrix


class ToyMLP:
    """
    Toy two layer MultiLayerPerceptron

    :cvar DEFAULT_PARAMS: dictionary of default (latest best) model parameters
    :cvar DEFAULT_FIT_PARAMS: dictionary of default (latest best) fit parameters
    :ivar logger: logger.
    :ivar model: model itself
    :ivar state: variable indicating whether the model was imported or trained or yet untrained
    :ivar seed: random seed on which the model random elements are drawn from.
    """
    # Params - update num_features if changing feature_df for hyper parametrisation.
    DEFAULT_MODEL_PARAMS = {
        'layer_num': 3, 'num_unit': 70, 'act_method': 'relu', 'optimizer': tf.keras.optimizers.Adam,
        'learning_rate': 0.01, 'dropout': None, 'grad_norm': 1, 'kernel_l1': 0.00001,
        'kernel_l2': 0.00001, 'activity_l1': 0.0001, 'activity_l2': 0.0001, 'shrink_rate': 1,
    }
    DEFAULT_FIT_PARAMS = {
        'epochs': 20, 'batch_size': 256, 'class_weight': 'balanced', 'validation_split': 0.2,
        'callbacks': [keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=5)],
    }

    def __init__(self, seed: int = 13, **model_params) -> None:
        """ Instantiate BIMLP.

        :param seed: random sed
        """
        self.model = None
        self.state = None
        self.logger = logging.getLogger('ToyMLP')
        self.seed = seed
        self.model_params = self.DEFAULT_MODEL_PARAMS
        self.model_params.update(model_params)

    def fit(
        self,
        train_features: pd.DataFrame,
        train_label: ArrayDataFrameMatrix,
        category_lst: list,
        **fit_params,
    ) -> None:
        """ Train model.

        :param category_lst: list of categories to predict
        :param train_features: features dataframe
        :param train_label: array of labels
        **kwargs
            :kwarg model_params: optional dict with model parameters. If not specified default used.
            :kwarg fit_params: optional dict with fit parameters. epochs, batch_size, and class_weight mandatory
        """
        # Handle kwargs
        default_params = self.DEFAULT_FIT_PARAMS.copy()
        default_params.update(fit_params)

        train_label = self.encode_labels(train_label, categories=category_lst)
        self.model = self.get_model(
            features=train_features, label=train_label, model_params=self.model_params,
        )

        # Fitting the network
        label_matrix = to_categorical(train_label, dtype='int64')  # Keras 1 hot encoding
        self.model.fit(train_features, label_matrix, **fit_params)
        self.state = 'trained'

    def get_model(
        self, features: pd.DataFrame, label: np.ndarray, model_params: dict,
    ):
        """
        Get Keras model

        :param features: features after scaling
        :param label: list of labels
        :param model_params: dictionary of model parameters
        :return KerasClassifier: model build with parameters
        """
        # Fix seeds - core python, numpy and tensorFlow level
        self.fix_seeds()
        model_params['num_features'] = features.shape[1]
        classes = np.unique(label)
        model_params['num_classes'] = len(classes)
        model_params['train_means'] = np.mean(features).tolist()
        model_params['train_variances'] = np.var(features).tolist()

        return KerasClassifier(build_fn=self.build_network, **model_params)

    def build_network(
        self, num_features: int, num_classes: int, layer_num: int, num_unit: int, act_method: str,
        learning_rate: float, optimizer: tf.optimizers.Optimizer, dropout: float, grad_norm: float,
        kernel_l1: float, kernel_l2: float, activity_l1: float, activity_l2: float,
        shrink_rate: float, train_means: list, train_variances: list,
    ):
        """
        Build the neural network architecture.

        :param num_features: int the number of features in the features dataframe
        :param num_classes: int the number of classes to predict (at least 2)
        :param layer_num: int the number of hidden layers in the network
        :param num_unit: int the number of neurons per layer (at least 1)
        :param act_method: string the activation method (sigmoid, relu...)
        :param learning_rate: float the share of gradient from which the weights will be updated
        :param optimizer: a keras compatible weight optimizer
        :param dropout: None or float - if None no drop out, else float (comprised between 0 and 1) is percentage
        dropout.
        :param grad_norm: maximum norm of the gradient; used for gradient clipping to avoid gradient explosion
        :param kernel_l1: L1 regularisation parameter on kernel weights
        :param kernel_l2: L2 regularisation parameter on kernel weights
        :param activity_l1: L1 regularisation parameter on layer output
        :param activity_l2: L2 regularisation parameter on layer output
        :param shrink_rate: rate at which we reduce the number of neurons in each hidden layer
        :param train_means: mean of each feature in the training dataset
        :param train_variances: variance of each feature in the training dataset
        :return model: keras sequential model
        """

        # Initial input layer and normalization (scaling) layer
        model = keras.Sequential()
        model.add(layers.InputLayer(input_shape=(num_features,)))
        model.add(tf.keras.layers.Normalization(mean=train_means, variance=train_variances))

        # Generate hidden layers
        for _layer in range(layer_num):
            if (dropout is not None) and (dropout > 0):
                model.add(layers.Dropout(rate=dropout, input_shape=(num_features,), seed=self.seed))

            if (kernel_l1 > 0) or (kernel_l2 > 0) or (activity_l1 > 0) or (activity_l2 > 0):
                model.add(layers.Dense(
                    units=num_unit, activation=act_method, input_shape=(num_features,),
                    kernel_initializer=tf.keras.initializers.GlorotNormal(seed=self.seed),
                    kernel_regularizer=tf.keras.regularizers.l1_l2(l1=kernel_l1, l2=kernel_l2),
                    activity_regularizer=tf.keras.regularizers.l1_l2(l1=activity_l1, l2=activity_l2),
                ))

            else:
                model.add(layers.Dense(
                    units=num_unit, activation=act_method, input_shape=(num_features,),
                    kernel_initializer=tf.keras.initializers.GlorotNormal(seed=self.seed),
                ))
            num_unit = int(round(shrink_rate * num_unit, 0))

        # Output layer - probability for each class
        model.add(layers.Dense(num_classes, activation='softmax'))

        # Build and compile model
        model.compile(
            loss='categorical_crossentropy',
            optimizer=optimizer(learning_rate=learning_rate, clipnorm=grad_norm),
        )
        return model

    @staticmethod
    def encode_labels(label: np.ndarray, categories: list) -> np.ndarray:
        """ Map labels AdvisoryCategory id to index in categories list.

        Models predict classes as column index with first class in first column. To ensure
        a consistent decoding of model predictions, we encode labels so that the first category
        in the categories list has index 0, then the second 1, etc... See the method decode labels
        of BiProdModel.

        :param label: numpy array of AdvisoryCategory ids
        :param categories: list of categories to predict

        :return: encoded labels numpy array with AdvisoryCategory ids converted to list/column index
        """
        unique_category_ids, category_indexes = np.unique(
            label,
            return_inverse=True,
        )

        # Convert string label to category indexes
        # For efficiency we loop over label unique values, convert them to categories list index
        # and broadcast the categories list index back to each unique idx in the array.
        # see https://stackoverflow.com/questions/16992713/translate-every-element-in-numpy-array-according-to-key
        category_ids_to_idx = {category.id: idx for idx, category in enumerate(categories)}
        unique_predicted_categories_ids = np.array(
            [category_ids_to_idx[category_id] for category_id in unique_category_ids]
        )
        return unique_predicted_categories_ids[category_indexes].reshape(label.shape)

    def fix_seeds(self):
        """ Fix seeds - core python, numpy and tensorFlow level """
        python_random.seed(self.seed)
        np.random.seed(self.seed)
        tf.random.set_seed(self.seed)
