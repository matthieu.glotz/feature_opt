"""
Configuration file for Pytest. Test fixtures defined here will be available for the tests modules
defined in this repo.
"""
from __future__ import annotations

from enum import Enum
from typing import Callable, List, Type

import numpy as np
import pandas as pd
import pytest
from sklearn.feature_extraction.text import TfidfVectorizer

from src.space.dimensions import CategoricalDim, PipelineDimension, IntegerDim, RealDim
from src.features.features import ArrayDataFrameMatrix, FeatureTemplate, generate_features_class


class TestCategories(Enum):
    """ Enum class listing the categories for the test tuning problem"""
    ECOMMERCE = 'ecommerce'
    SALARY = 'salary'
    ENERGY = 'energy'
    OTHER = 'other'


@pytest.fixture
def simple_transform_function():
    """ Pytest fixture returning a simple transform function to test features generation with."""
    def transform_func(feature_instance: FeatureTemplate, raw_data: ArrayDataFrameMatrix):
        return raw_data
    return transform_func


@pytest.fixture
def simple_fit_function():
    """ Pytest fixture returning a simple fit function to test features generation with."""
    def fit_func(feature_instance: FeatureTemplate, raw_data: ArrayDataFrameMatrix):
        pass
    return fit_func


@pytest.fixture
def incorrect_transform():
    """ Pytest fixture returning an erroneous transform function to test features generation error
    handling.
    """
    def transform_func(raw_data: ArrayDataFrameMatrix):
        return raw_data
    return transform_func


@pytest.fixture
def incorrect_fit():
    """ Pytest fixture returning an erroneous fit function to test features generation error
    handling.
    """
    def fit_func(feature_instance: FeatureTemplate):
        pass
    return fit_func


@pytest.fixture
def complex_fit_function():
    """ Pytest fixture returning a complex fit function to test features generation with."""
    def fit_func(
        feature_instance: FeatureTemplate,
        raw_data: pd.DataFrame,
        no_na: bool = True,
        max_ngram_len: int = 1,
    ) -> None:
        """ Complex fit function.

        :param feature_instance: a Feature class with fit, transform, and fit_transform functions
        :param raw_data: a pandas DataFrame containing a value and description columns
        :param no_na: boolean; if True raises an error if any nan values present in the data
        :param max_ngram_len: integer, max number of words in the TfIDFVectorizer ngrams

        :raises ValueError: if any Nan value present in the data
        """
        if no_na and (pd.isna(raw_data).any()).any():
            raise ValueError(
                'The passed raw data has null values whilst none are allowed'
            )

        if "description" not in raw_data.columns or "value" not in raw_data.columns:
            raise ValueError(
                'The passed raw data should have a description and a value column'
            )

        # TFIDF representation of description
        vectorizer = TfidfVectorizer(ngram_range=(1, max_ngram_len))
        vectorizer.fit(raw_data['description'])
        feature_instance.vectorizer = vectorizer

        # Store training data mean value and value standard deviation for normalization during transform
        feature_instance.mean_value = raw_data['value'].mean()
        feature_instance.std_value = raw_data['value'].std()

    return fit_func


@pytest.fixture
def complex_transfom_function():
    """ Pytest fixture returning a complex transform function to test features generation with."""
    def transform_func(
        feature_instance: FeatureTemplate,
        raw_data: pd.DataFrame,
        value_threshold: float,
    ) -> np.ndarray:
        """ Complex fit function.

        :param feature_instance: a Feature class with fit, transform, and fit_transform functions
        :param raw_data: a pandas DataFrame containing a value and description columns
        :param value_threshold: a threshold beyond which a dummy value for high amount is added
        """

        if "description" not in raw_data.columns or "value" not in raw_data.columns:
            raise ValueError(
                'The passed raw data should have a description and a value column'
            )

        # TFIDF representation of description
        tfidf_matrix = feature_instance.vectorizer.transform(raw_data['description']).toarray()

        # Generate high value dummy
        raw_data['high_value_dummy'] = np.where(raw_data['value'] > value_threshold, 1, 0)

        # Normalize value data using standard deviation and mean values stored during transform
        raw_data['value'] = (
            raw_data['value'] - feature_instance.mean_value
        ) / feature_instance.std_value

        return np.concatenate(
            [raw_data[['value', 'high_value_dummy']].to_numpy(), tfidf_matrix],
            axis=1,
        )

    return transform_func


@pytest.fixture
def test_raw_data() -> pd.DataFrame:
    """ Test data to test logic on - mimicking a banking transaction categorization problem."""
    return pd.DataFrame(
        [
            [-125.6, 'Amazon.com purchase'],
            [3256.35, 'Net Salary for March'],
            [15, 'Lydia Elisabeth'],
            [-126.3, 'Bill Engie 2023-02-31 - customer XDJ23546'],
        ],
        columns=['value', 'description'],
    )


@pytest.fixture
def test_train_label() -> pd.Series:
    """ Training data labels."""
    return pd.Series(
        [
            TestCategories.ECOMMERCE.value,
            TestCategories.SALARY.value,
            TestCategories.OTHER.value,
            TestCategories.ENERGY.value,
        ],
        name='true_category',
    )


@pytest.fixture
def test_score_data() -> pd.DataFrame:
    return pd.DataFrame(
        [
            [-47.8, 'Vinted.com - order ADJ23145'],
            [2840.35, 'VIR POWENS Matthieu GLOTZ January'],
            [-126.3, 'PREL EDF - BILL JAN 2024'],
            [-12.5, 'CB *1754 MC DONALDS'],
            [-550.5, 'VIR M BREHIERE MARCEL MOTIF: Remboursement'],
        ],
        columns=['value', 'description'],
    )


@pytest.fixture
def test_score_label() -> pd.Series:
    """ Training data labels."""
    return pd.Series(
        [
            TestCategories.ECOMMERCE.value,
            TestCategories.SALARY.value,
            TestCategories.ENERGY.value,
            TestCategories.OTHER.value,
            TestCategories.OTHER.value,
        ],
        name='true_category',
    )


@pytest.fixture
def feature_class(
    complex_fit_function: Callable,
    complex_transfom_function: Callable,
) -> Type[FeatureTemplate]:
    """ Return a Feature class for tuner testing."""
    return generate_features_class(
        fit_function=complex_fit_function,
        transform_function=complex_transfom_function,
    )


@pytest.fixture
def features_search_space() -> List[PipelineDimension]:
    return [
        IntegerDim(low=1, high=3, name='max_ngram_len', dim_type='features_fit'),
        RealDim(low=0, high=1000, name='value_threshold', dim_type='features_transform'),
    ]


@pytest.fixture
def rf_search_space() -> List[PipelineDimension]:
    return [
        IntegerDim(low=1, high=5, name='n_estimators', dim_type='model'),
        CategoricalDim(
            categories=['gini', 'log_loss', 'entropy'],
            name='criterion',
            dim_type='model',
        ),
    ]


@pytest.fixture
def mlp_search_space() -> List[PipelineDimension]:
    return [
        RealDim(low=0, high=1, name='dropout', dim_type='model'),
        IntegerDim(low=4, high=32, name='num_unit', dim_type='model'),
        IntegerDim(low=20, high=80, name='epochs', dim_type='model_fit'),
    ]
