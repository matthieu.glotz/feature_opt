""" Module to unit test the HyperParamTuner class.

Validates:
    - The tuner works using one or several of the 4 dimensions type available
    - That the tuner params and minimizer params can be updated
    - That different models can be used
"""
from __future__ import annotations

from logging import getLogger
from typing import TYPE_CHECKING, List, Type

import pandas as pd
from sklearn.ensemble import RandomForestClassifier

from src.tuner import HyperParamTuner
from .toy_models import ToyMLP

if TYPE_CHECKING:
    from src.features.features import FeatureTemplate
    from src.space.dimensions import PipelineDimension


def test_dimensions_tuning(
    test_raw_data: pd.DataFrame,
    test_train_label: pd.Series,
    test_score_data: pd.DataFrame,
    test_score_label: pd.Series,
    feature_class: Type[FeatureTemplate],
    features_search_space: List[PipelineDimension],
    rf_search_space: List[PipelineDimension],
    mlp_search_space: List[PipelineDimension],
):
    for model, model_space in [
        (RandomForestClassifier, rf_search_space),
        (ToyMLP, mlp_search_space),
    ]:
        tuner = HyperParamTuner(
            feature_class=feature_class,
            logger=getLogger('test'),
            train_data=test_raw_data,
            train_label=test_train_label,
            score_data=test_score_data,
            score_label=test_score_label,
            model_class=model,
            search_space=features_search_space + model_space
        )
        tuner.optimize()
