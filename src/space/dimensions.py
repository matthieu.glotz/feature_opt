""" Hyper parametrization space dimension definition"""
from __future__ import annotations

from abc import ABC, abstractmethod
from typing import List

from skopt.space import Categorical, Dimension, Integer, Real


class PipelineDimension(Dimension, ABC):
    """
    Abstract dimension class. Variant of Scikit Optimize Dimension mother class.

    We add to skopt Dimension class a type attribute to differentiate features param
    from fit params and model params. A Dimension is here defined as a named hyperparameter axis
    range; an hyperparameter as a value drawn from this axis; and a search space as a list of space
    over which we want to optimize the model.
    """
    dim_type_lst = [
        'features_fit',
        'features_transform',
        'model_fit',
        'model',
    ]

    @abstractmethod
    def __init__(self, dim_type: str) -> None:
        """
        Instantiate PipelineDimension

        :param dim_type: parameter type (features_fit, features_transform, model_fit, model)
        """
        assert dim_type in self.dim_type_lst, f'Param type must be chosen from {self.dim_type_lst}'
        self.dim_type = dim_type


class IntegerDim(Integer, PipelineDimension):
    """
    Variant of Scikit Optimize Integer class.

    We add to skopt Integer class a type attribute to differentiate features params from fit params
    and model params. A Dimension is here defined as a named hyperparameter axis range;
    an hyperparameter as a value drawn from this axis;
    and a search space as a list of space over which we want to optimize the model.
    """

    def __init__(self, low: int, high: int, name: str, dim_type: str) -> None:
        """
        Instantiate IntegerDim

        :param low: parameter lower bound (included)
        :param high: parameter upper bound (included)
        :param name: string parameter name
        :param dim_type: parameter type (features_fit, features_transform, model_fit, model)
        """
        Integer.__init__(self, low=low, high=high, name=name)
        PipelineDimension.__init__(self, dim_type=dim_type)


class RealDim(Real, PipelineDimension):
    """
    Variant of Scikit Optimize Real class.

    We add to skopt Real class a type attribute to differentiate features params from fit params
    and model params. A Dimension is here defined as a named hyperparameter axis range;
    an hyperparameter as a value drawn from this axis;
    and a search space as a list of space over which we want to optimize the model.
    """

    def __init__(self, low: float, high: float, name: str, dim_type: str) -> None:
        """
        Instantiate RealDim

        :param low: parameter lower bound (included)
        :param high: parameter upper bound (included)
        :param name: string parameter name
        :param dim_type: parameter type (features_fit, features_transform, model_fit, model)
        """
        Real.__init__(self, low=low, high=high, name=name)
        PipelineDimension.__init__(self, dim_type=dim_type)


class CategoricalDim(Categorical, PipelineDimension):
    """
    Variant of Scikit Optimize Categorical class.

    We add to skopt Categorical class a type attribute to differentiate features params from fit
    params and model params. A Dimension is here defined as a named hyperparameter axis range;
    an hyperparameter as a value drawn from this axis; and a search space as a list of space
    over which we want to optimize the model.
    """

    def __init__(
        self, categories: List, name: str, dim_type: str, transform: str = 'onehot',
    ) -> None:
        """
        Instantiate CategoricalDim

        :param categories: list of categorical choices. Element of the list can be of any type.
        :param name: string parameter name
        :param dim_type: parameter type (features_fit, features_transform, model_fit, model)
        """
        Categorical.__init__(self, categories=categories, name=name, transform=transform)
        PipelineDimension.__init__(self, dim_type=dim_type)
