""" Features class module

Generates scikit-learn style Features object from a raw DataFrame and, optionally, transform and
fit functions to, respectively, map this raw data into one or multiple features dataframes and
train/fit on a training set.
"""
from __future__ import annotations

import inspect
from abc import ABC, abstractmethod
from typing import Callable, Iterable, Optional, Type, Union

import numpy as np
import pandas as pd
from scipy.sparse import spmatrix

ArrayDataFrameMatrix = Union[np.ndarray, pd.DataFrame, pd.Series, spmatrix]


class FeatureTemplate(ABC):
    """ Abstract FeatureTemplate class.

    Inheriting from this class allows to build custom Feature classes which are compatible with
    feature opt Tuner.
    """

    def __init__(self):
        pass

    @abstractmethod
    def fit(self, raw_data: ArrayDataFrameMatrix, *args, **kwargs) -> None:
        pass

    @abstractmethod
    def transform(self, raw_data: ArrayDataFrameMatrix, *args, **kwargs) -> ArrayDataFrameMatrix:
        return raw_data


class ArgumentError(Exception):
    def __init__(self, message: str):
        super().__init__(message)


def default_fit(self, raw_data: ArrayDataFrameMatrix, *args, **kwargs):
    """ Default fit function. No internal parameter is changed."""
    pass


def default_transform(self, raw_data: ArrayDataFrameMatrix, *args, **kwargs):
    """ Default transform function. Raw data returned as is."""
    return raw_data


def generate_features_class(
    fit_function: Optional[Callable] = default_fit,
    transform_function: Optional[Callable] = default_transform,
    trainable_args: Iterable[str] = tuple(),
) -> Type[FeatureTemplate]:
    """ Scikit Learn style Features class factory.

    :param fit_function: callable accepting a FeaturesGenerator as a first argument and raw data
    as a numpy array or pandas DataFrame as second argument, and additional optional tunable fit
    parameters as additional params. If not provided, calling fit simply returns the original raw
    data.
    :param transform_function: callable accepting a FeaturesGenerator as a first argument and
    raw data as a numpy array or pandas DataFrame as a second argument, and additional trainable
    and tunable fit parameters as additional params. If not provided, calling transform simply
    passes.
    :param trainable_args: list of trainable arguments, to be stored as instance attributes

    :return: a FeatureClass, child of FeatureTemplate, compatible with feature_opt tuner
    """
    # Inspect fit and transform function
    for name, function_argument in (('fit', fit_function), ('transform', transform_function)):

        if not callable(function_argument):
            raise TypeError(f'The {name} must be callable')

        function_arguments = inspect.getfullargspec(function_argument).args
        if len(function_arguments) < 2:
            raise ArgumentError(
                f'The {name} must accept at least two arguments: a Feature class and a data array'
            )

    class Features(FeatureTemplate):
        """ Scikit style features class based on the passed fit function and transform function."""
        def __init__(self):
            """ Instantiate the Features."""
            super().__init__()
            for arg in trainable_args:
                setattr(self, arg, None)  # Pass None as default value until transform updates it

        def transform(self, raw_data: ArrayDataFrameMatrix, *args, **kwargs):
            """ Transform original raw_data into features."""
            return transform_function(self, raw_data, *args, **kwargs)

        def fit(self, raw_data: ArrayDataFrameMatrix, *args, **kwargs):
            """ Fit the training data to update internal parameters in future transform calls."""
            return fit_function(self, raw_data, *args, **kwargs)

    return Features
