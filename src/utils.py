"""General utility function and classes for pipeline hyperparametrisation"""
import signal
from contextlib import contextmanager


@contextmanager
def time_limit(seconds: int):
    """ Sets a time limit to a function execution

    :param seconds: time limit in seconds

    :raises AssertionError: if time limit is reached
    """
    def signal_handler(signum, frame):
        raise AssertionError("Timed out!")
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)
