# pipeline_opt
Scikit Opt based hyperparametrisation framework optimising feature parameters jointly with model and fit parameters

## Problem statement

Existing hyperparameter tuning framework, such as scikit-optimise, provide a wide range of technique for model parameter optimisation, ranging from brute-force grid search to more advanced Bayesian Optimization search techniques. However, the existing libraries focus solely on model and fit parameters - no support is provided to optimize feature selection and features parameters. This gap is particularly relevant as feature selection and parameters have been shown by research to both have a significant impact on model performance but also on the selection of model hyperparameters themselves (see Jie, Jiawei 2018); with the litterature describing how this can be particularly relevant for NLP (see Daelemans, Hoste, et al 2010).

This project aims to answer this by providing a framework to jointly optimize feature extraction parameters, feature selection, model fitting and model parameters, and even data sampling, using the latest Bayesian Optimization search techniques.

## Framework

This project builds on Scikit Opt optimization framework, by which parameters are associated with a dimension in the optimization space, feature parameters being just another dimensions amongst the other parameters being optimized. The entire prediction pipeline is rerun when using new feature parameters combinations, but for optimization the algorithm caches past feature combinations not to recalculate features if using the same features parameters with different model parameters. Optimization is operated by default using Scikit-Optimizer Gaussian Process minimizer, but can handle any Scikit-Opt style optimizer.

The overall process for an hyperparametrization_run is as follows: 
    
    (i) Initialise by randomly drawing n_initial_points (parameter from optimizer_params) from the search space and
    evaluate bi_model scores at these points using the evaluate_model method.
    
    (ii) Then use points scores to fit a surrogate model proxying the posterior distribution of scores to
    hyperparameters.
    
    (iii) Acquire the optimal point of the surrogate model as the next set of hyperparameter values to evaluate.
    
    (iv) Evaluate the score of the optimal point using the evaluate_model method (back to step (i)). The loop ends when
    the number of points evaluated reaches n_samples.

## Practical use

First define a hyperparametrisation space, i.e. a list of dimensions (e.g. feature or model parameters) over which the model pipeline should be optimized:
```
optimization_params = [
	IntegerDim(low=1, high=3, name='max_ngram_len', dim_type='features_fit'),
	RealDim(low=0, high=1000, name='value_threshold', dim_type='features_transform'),
	IntegerDim(low=10, high=50, name='n_estimators', dim_type='model'),
	CategoricalDim(
		categories=['gini', 'log_loss', 'entropy'],
		name='criterion',
		dim_type='model',
	)
]
```
Second define a FeatureClass, e.g. a class implementing a fit function, to fit the training set if required, and transform, to compute the features as an Array or DataFrame from an Array of raw data.
You may use the helper `generate_feature_class` to do so:
```
FeaturesClass = generate_features_class(
	fit_function=fit_function,
	transform_function=transform_function,
)
```

Second call the HyperparamTuner object, pass it the pipeline parameters, and call it's optimize method to get back the optimal feature, fit, model, and sampling parameters.
```
tuner = HyperParamTuner(
    feature_class=FeaturesClass,
    logger=getLogger('test'),
    train_data=test_raw_data,
    train_label=test_train_label,
    score_data=test_score_data,
    score_label=test_score_label,
    model_class=model,
    search_space=features_search_space + model_space
)
feature_params, fit_params, model_params, sampling_params = tuner.optimize()  # Optimal params after Bayesian HyperParameter Parametrization
```

## Installation
git clone the current projet using either ssh or https:

ssh:
```
git clone git@gitlab.com:matthieu.glotz/feature_opt.git
```

https:
```
git clone https://gitlab.com/matthieu.glotz/feature_opt.git
```

then install the package using pip:
```
cd feature_opt
pip install .
```
